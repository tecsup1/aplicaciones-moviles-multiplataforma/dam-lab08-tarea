import React, {Component} from 'react';
import {View, Text, Image, Alert} from 'react-native';

import TabMenuContainer from './TabMenuContainer';

// import InicioView from './inicioComponents/InicioView';
import PedidoView from './pedidosComponents/PedidoView';
// import ReservasView from './reservasComponents/ReservasView';

// react navigation:
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, HeaderBackButton} from '@react-navigation/stack';
const Stack = createStackNavigator();

import PedidosView from './pedidosComponents/PedidosView';
import MapViewComponent from './pedidosComponents/MapView';

class MainStackContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          {/* <Stack.Screen
            name="TabMenuContainer"
            component={TabMenuContainer}
            options={{
              title: 'EasyFood',
              headerTintColor: 'white',
              headerStyle: {
                backgroundColor: '#568B2F',
              },
            }}
          /> */}

          {/* DESPUES DE ESTA LINEA AÑADIR SUS COMPONENTES AL STACK*/}

          <Stack.Screen
            name="PedidosView"
            component={PedidosView}
            options={{
              title: 'Pedido View',
              headerTintColor: 'white',
              headerStyle: {
                backgroundColor: '#568B2F',
              },
            }}
          />

          <Stack.Screen
            name="PedidoView"
            component={PedidoView}
            options={{
              title: 'Pedido View',
              headerTintColor: 'white',
              headerStyle: {
                backgroundColor: '#568B2F',
              },
            }}
          />

          <Stack.Screen
            name="MapViewComponent"
            component={MapViewComponent}
            options={{
              title: 'Map View',
              headerTintColor: 'white',
              headerStyle: {
                backgroundColor: '#568B2F',
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default MainStackContainer;
