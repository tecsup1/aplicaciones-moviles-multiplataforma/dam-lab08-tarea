import React, {Component} from 'react';
import {View, Text, Image, Alert} from 'react-native';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();

import InicioView from './inicioComponents/InicioView';
import PedidosView from './pedidosComponents/PedidosView';
import ReservasView from './reservasComponents/ReservasView';
import 'react-native-gesture-handler';

class TabMenuContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: 'black',
          inactiveTintColor: 'white',
          indicatorStyle: {
            backgroundColor: 'white',
          },
          style: {
            backgroundColor: '#568B2F',
          },
          swipeEnabled: 'false'
        }}
        screenOptions={{
          activeTintColor: 'blue',
        }}
        swipeEnabled={false}
        
        
        >
        <Tab.Screen name="InicioView" component={InicioView} />
        <Tab.Screen name="ReservasView" component={ReservasView} />
        <Tab.Screen name="PedidosView" component={PedidosView} />
        
      </Tab.Navigator>
    );
  }
}

export default TabMenuContainer;
