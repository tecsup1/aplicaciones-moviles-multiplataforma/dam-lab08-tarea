/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/views/MainStackContainer';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
